#ifndef _CHUNK_LIST_H_
#define _CHUNK_LIST_H_

#include <stdlib.h>
#include "Handlers.h"

#define DEFAULT_CHUNK_SIZE 16

typedef struct ChunkList {
    struct ChunkList *next;
    unsigned int count;
    char **elems;
} ChunkList;

ChunkList *chunk_list_init(void);
void chunk_list_free(ChunkList *ch);
void chunk_list_foreach(ChunkList *ch, VoidPtrHandler f);
void chunk_list_insert(ChunkList *ch, void *ptr);

#endif
