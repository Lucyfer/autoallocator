#include <stdio.h>
#include <string.h>
#include "Allocator.h"

/// Sample Usage
int main() {
    char str[] = "Hello world";
    char str2[] = "Hello from nested";
    char *allocated;
    char *nested;
    initialize();

    region_begin();
    allocated = (char *)alloc_local(strlen(str) + 1);
    memcpy(allocated, str, strlen(str) + 1);
    printf("String: %s\n", allocated);

        region_begin(); // begin nested region
        nested = (char *)alloc_local(strlen(str2) + 1);
        strcpy(nested, str2);
        printf("Nested: %s\n", nested);
        region_end(); // end nested region

        printf("Nested: %s\n", nested); // empty
    region_end(); // dealloc external region

    printf("String: %s\n", allocated); // empty

    destroy();
    return 0;
}
