#include "Allocator.h"

/// Private fields

static Stack _globalStack;
static HashSet _globalSet;

/// Public methods

void initialize(void) {
    hashset_init(&_globalSet);
}

void destroy(void) {
    hashset_foreach(&_globalSet, free);
    hashset_clear(&_globalSet);
    stack_clear(&_globalStack);
}

void region_begin(void) {
    ChunkList *ch = chunk_list_init();
    stack_push(&_globalStack, ch);
}

bool region_end(void) {
    ChunkList *ch = (ChunkList *)stack_pop(&_globalStack);
    if (ch) {
        chunk_list_foreach(ch, dealloc);
        chunk_list_free(ch);
        return true;
    }
    return false;
}

void *alloc_local(const size_t size) {
    char *buf = (char *)malloc(size);
    ChunkList *ch = (ChunkList *)stack_peek(&_globalStack);
    chunk_list_insert(ch, buf);
    hashset_insert(&_globalSet, buf);
    return buf;
}

void *alloc_global(const size_t size) {
    char *buf = (char *)malloc(size);
    hashset_insert(&_globalSet, buf);
    return buf;
}

void dealloc(void *ptr) {
    if (hashset_contains(&_globalSet, (char *)ptr)) {
        hashset_remove(&_globalSet, (char *)ptr);
        free(ptr);
    }
}
