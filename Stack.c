#include "Stack.h"

Stack *stack_init(void) {
    Stack *s = (Stack *)malloc(sizeof(Stack));
    s->elems = (char **)malloc(sizeof(char *) * DEFAULT_STACK_SIZE);
    s->top = -1;
    s->size = DEFAULT_STACK_SIZE;
    return s;
}

void stack_clear(Stack *s) {
    free(s->elems);
    s->elems = NULL;
    s->top = -1;
    s->size = 0;
}

void stack_push(Stack *s, void *ptr) {
    if ((unsigned int)s->top + 1 < s->size)
    {
        s->top++;
        s->elems[s->top] = (char *)ptr;
    } else {
        s->size += DEFAULT_STACK_SIZE;
        s->elems = (char **)realloc(s->elems, sizeof(char *) * s->size);
        stack_push(s, ptr);
    }
}

void *stack_pop(Stack *s) {
    if (s->top >= 0) {
        void *tmp = s->elems[s->top];
        s->top--;
        return tmp;
    }
    return NULL;
}

void *stack_peek(const Stack *s) {
    if (s->top >= 0) {
        return s->elems[s->top];
    }
    return NULL;
}
