#ifndef _HASH_SET_H_
#define _HASH_SET_H_

#include <stdlib.h>
#include <stdbool.h>
#include "Handlers.h"

#define DEFAULT_HASHSET_SIZE 256
#define HASHSET_MAX_ATTEMPTS 7

typedef struct HashSet {
    unsigned int size;
    unsigned int count;
    char **elems;
} HashSet;

HashSet *hashset_alloc(void);
HashSet *hashset_init(HashSet *h);
void hashset_clear(HashSet *h);
void hashset_destroy(HashSet *h);
void hashset_insert(HashSet *h, char *elem);
void hashset_remove(HashSet *h, const char *elem);
bool hashset_contains(const HashSet *h, const char *elem);
void hashset_foreach(const HashSet *h, VoidPtrHandler f);

#endif
