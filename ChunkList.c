#include "ChunkList.h"

/// Private method declaration

ChunkList *chunk_init(void);

/// Private methods implementation

ChunkList *chunk_init(void) {
    ChunkList *ch = (ChunkList *)malloc(sizeof(ChunkList));
    ch->count = 0;
    ch->elems = (char **)malloc(sizeof(char *) * DEFAULT_CHUNK_SIZE);
    ch->next = NULL;
    return ch;
}

/// Public methods

ChunkList *chunk_list_init(void) {
    return chunk_init();
}

void chunk_list_free(ChunkList *ch) {
    ChunkList *current = ch;
    while (current) {
        ChunkList *next = current->next;
        free(current->elems);
        free(current);
        current = next;
    }
}

void chunk_list_foreach(ChunkList *ch, VoidPtrHandler f) {
    ChunkList *current = ch;
    unsigned int i;
    while (current) {
        for (i = 0; i < current->count; ++i) {
            f(current->elems[i]);
        }
        current = current->next;
    }
}

void chunk_list_insert(ChunkList *ch, void *ptr) {
    if (ch) {
        if (ch->count < DEFAULT_CHUNK_SIZE) {
            ch->elems[ch->count] = (char *)ptr;
            ch->count++;
        } else {
            if (!ch->next) {
                ch->next = chunk_init();
            }
            chunk_list_insert(ch->next, ptr);
        }
    }
}
