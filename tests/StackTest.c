#include <stdio.h>
#include <assert.h>

#include "../Stack.h"

void stack_init_test(void) {
    printf("Init test\n");
    Stack *s = stack_init();

    assert(s->elems != NULL);
    assert(s->top == -1);
    assert(s->size == DEFAULT_STACK_SIZE);
    stack_clear(s);
}

void stack_clear_test(void) {
    printf("\nClear test\n");
    printf("Stage 1:\n");
    Stack *s = stack_init();
    stack_clear(s);

    assert(s->elems == NULL);
    assert(s->top == -1);
    assert(s->size == 0);

    printf("Stage 2:\n");
    s = stack_init();
    for (int i = 0; i < 1000; ++i) {
        stack_push(s, (void *)i);
    }
    stack_clear(s);

    assert(s->elems == NULL);
    assert(s->top == -1);
    assert(s->size == 0);

    printf("Stage 3:\n");
    s = stack_init();
    for (int i = 0; i < 1000; ++i) {
        stack_push(s, (void *)i);
        stack_pop(s);
    }
    stack_clear(s);

    assert(s->elems == NULL);
    assert(s->top == -1);
    assert(s->size == 0);
}

void stack_push_test(void) {
    printf("\nPush test\n");
    Stack *s = stack_init();
    for (int i = 0; i < 1000; ++i) {
        stack_push(s, (void *)i);
        assert(i == (int)s->elems[i]);
    }
    assert(s->top == 999);
    stack_clear(s);
}

void stack_pop_test(void) {
    printf("\nPop test\n");
    Stack *s = stack_init();
    assert(stack_pop(s) == NULL);

    for (int i = 1; i < 1000; ++i) {
        stack_push(s, (void *)i);
    }
    for (int i = 999; i >= 1; --i) {
        assert(i == (int)stack_pop(s));
    }
    assert(s->top == -1);
    assert(stack_pop(s) == NULL);
    stack_clear(s);
}

void stack_peek_test(void) {
    printf("\nPeek test\n");
    Stack *s = stack_init();
    assert(stack_peek(s) == NULL);
    for (int i = 1; i < 1000; ++i) {
        stack_push(s, (void *)i);
        assert(i == (int)stack_peek(s));
    }
    for (int i = 999; i >= 1; --i) {
        assert(i == (int)stack_peek(s));
        stack_pop(s);
    }
    stack_clear(s);
}

int main(int argc, char const *argv[]) {
    stack_init_test();
    stack_clear_test();
    stack_push_test();
    stack_pop_test();
    stack_peek_test();
    return 0;
}
