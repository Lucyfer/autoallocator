#include <stdio.h>
#include <assert.h>
//#include <sys/resource.h>

#include "../ChunkList.h"

void chunk_list_init_test(void) {
    printf("Init test:\n");
    ChunkList *ch = chunk_list_init();

    assert(ch != NULL);
    assert(ch->next == NULL);
    assert(ch->count == 0);
    assert(ch->elems != NULL);
}

void chunk_list_free_test(void) {
    printf("\nFree test:\n");
    for (int i = 0; i < 1000; ++i) {
        ChunkList *ch = chunk_list_init();
        chunk_list_free(ch);
    }
    // todo getrusage();
}

static int counter;

void handler(void *ptr) {
    counter++;
    printf("Ptr: [%p] ", ptr);
    if ((unsigned int)ptr > 1000)
        printf("String: [%s]", (unsigned int)ptr == 0xDEADBEEF ? (char*)"0xDEADBEEF" : (char*)ptr);
    printf("\n");
}

void chunk_list_foreach_test() {
    printf("\nForeach test:\n");
    printf("Stage 1\n");
    counter = 0;
    ChunkList *ch = chunk_list_init();
    chunk_list_foreach(ch, handler);
    assert(counter == 0);

    printf("Stage 2\n");
    counter = 0;
    ch->elems[ch->count] = (char *)"Hello";
    ch->count++;
    ch->elems[ch->count] = (char *)"World";
    ch->count++;
    ch->elems[ch->count] = (char *)0xDEADBEEF;
    ch->count++;
    chunk_list_foreach(ch, handler);
    assert(counter == 3);

    printf("Stage 3\n");
    counter = 0;
    for (int i = 0; i < DEFAULT_CHUNK_SIZE; ++i) {
        chunk_list_insert(ch, (void *)i);
    }
    chunk_list_foreach(ch, handler);
    assert(counter == 3 + DEFAULT_CHUNK_SIZE);
}

void chunk_list_insert_test() {
    printf("\nInsert test: \n");
    ChunkList *ch = chunk_list_init();
    for (unsigned int i = 0; i < DEFAULT_CHUNK_SIZE + 5; ++i) {
        chunk_list_insert(ch, (void *)i);
    }
    for (unsigned int i = 0; i < DEFAULT_CHUNK_SIZE; ++i) {
        printf("[%u] = [%p]\n", i, ch->elems[i]);
        assert((unsigned int)ch->elems[i] == i);
    }
    assert(ch->next != NULL);
    for (unsigned int i = 0; i < 5; ++i) {
        printf("[%u] = [%p]\n", DEFAULT_CHUNK_SIZE + i, ch->next->elems[i]);
        assert((unsigned int)ch->next->elems[i] == DEFAULT_CHUNK_SIZE + i);
    }
}

int main(int argc, char const *argv[]) {
    chunk_list_init_test();
    chunk_list_free_test();
    chunk_list_foreach_test();
    chunk_list_insert_test();
    return 0;
}
