#include <stdio.h>
#include <assert.h>

#include "../HashSet.h"

void handler(void *ptr) {
    int p = (int)ptr;
    if (p < 1 || p > 999) {
        printf("Unexpected value [%p]\n", ptr);
        assert(false);
    }
}

static int counter;

void handler2(void *ptr) {
    counter++;
    int p = (unsigned int)ptr;
    switch (p) {
    case 0xDEADBEEF: printf("0xDEADBEEF\n"); break;
    case 0xFACED101: printf("0xFACED101\n"); break;
    default: printf("Unexprected [%p]\n", ptr);
    }
}

void hashset_alloc_test(void) {
    printf("Alloc test:\n");
    HashSet *h = hashset_alloc();
    assert(h != NULL);
    assert(h->size == 0);
    assert(h->count == 0);
    assert(h->elems == NULL);
    hashset_destroy(h);
}

void hashset_init_test(void) {
    printf("\nInit test:\n");
    HashSet *h = hashset_init(hashset_alloc());
    assert(h->elems != NULL);
    assert(h->size == DEFAULT_HASHSET_SIZE);
    assert(h->count == 0);
    hashset_destroy(h);
}

void hashset_clear_test(void) {
    printf("\nClear test:\n");
    HashSet *h = hashset_init(hashset_alloc());
    hashset_clear(h);
    assert(h->size == 0);
    assert(h->count == 0);
    assert(h->elems == NULL);
    hashset_foreach(h, handler);
    hashset_destroy(h);
}

void hashset_insert_test(void) {
    printf("\nInsert test:\n");
    HashSet *h = hashset_init(hashset_alloc());
    for (int i = 0; i < 1000; ++i) {
        hashset_insert(h, (char *)i);
    }
    printf("Count=%u\n", h->count);
    for (int i = 1; i < 1000; ++i) {
        assert(hashset_contains(h, (char *)i));
    }
    assert(h->count == 999);
    hashset_foreach(h, handler);
    hashset_destroy(h);
}

void hashset_remove_test(void) {
    printf("\nRemove test:\n");
    HashSet *h = hashset_init(hashset_alloc());
    for (int i = 0; i < 1000; ++i) {
        hashset_insert(h, (char *)i);
    }
    for (int i = 999; i >= 1; --i) {
        hashset_remove(h, (char *)i);
    }
    assert(h->count == 0);
    hashset_foreach(h, handler);
    hashset_destroy(h);
}

void hashset_contains_test(void) {
    printf("\nContains test:\n");
    HashSet *h = hashset_init(hashset_alloc());
    assert(hashset_contains(h, NULL) == false);

    for (int i = 0; i < 1000; ++i) {
        hashset_insert(h, (char *)i);
    }
    assert(hashset_contains(h, NULL) == false);
    for (int i = 1; i < 1000; ++i) {
        assert(hashset_contains(h, (char *)i));
    }
    for (int i = 1; i < 1000; ++i) {
        hashset_remove(h, (char *)i);
    }
    for (unsigned int i = 0; i < h->size; ++i) {
        assert(h->elems[i] == NULL);
    }
    hashset_destroy(h);
}

void hashset_foreach_test(void) {
    printf("\nForeach test:\n");

    HashSet *h = NULL;
    hashset_foreach(h, handler);

    h = hashset_init(hashset_alloc());
    hashset_foreach(h, handler);

    for (int i = 0; i < 1000; ++i) {
        hashset_insert(h, (char *)0xDEADBEEF);
        hashset_insert(h, (char *)0xFACED101);
        hashset_insert(h, (char *)0);
    }
    counter = 0;
    hashset_foreach(h, handler2);
    assert(counter == 2);

    hashset_destroy(h);
}

int main(int argc, char const *argv[]) {
    hashset_alloc_test();
    hashset_init_test();
    hashset_clear_test();
    hashset_insert_test();
    hashset_remove_test();
    hashset_contains_test();
    hashset_foreach_test();
    return 0;
}
