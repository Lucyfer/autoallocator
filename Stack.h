#ifndef _STACK_H_
#define _STACK_H_

#include <stdlib.h>

#define DEFAULT_STACK_SIZE 128

typedef struct Stack {
    unsigned int size;
    int top;
    char **elems;
} Stack;

Stack *stack_init(void);
void stack_clear(Stack *s);
void stack_push(Stack *s, void *ptr);
void *stack_pop(Stack *s);
void *stack_peek(const Stack *s);

#endif
