CC=clang
CFLAGS=-O2 -Weverything -c
LDFLAGS=
SOURCES=ChunkList.c HashSet.c Stack.c Allocator.c Main.c
OBJS=$(SOURCES:.c=.o)
EXECUTABLE=Main

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@

analyze:
	$(CC) --analyze $(SOURCES)

clean:
	rm -rfv $(EXECUTABLE) *.o ./**/*.o *.plist ./**/*.plist
