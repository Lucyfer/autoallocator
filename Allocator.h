#ifndef _ALLOCATOR_H_
#define _ALLOCATOR_H_

#include <stdlib.h>
#include "HashSet.h"
#include "Stack.h"
#include "ChunkList.h"

void initialize(void);
void destroy(void);
void region_begin(void);
bool region_end(void);
void *alloc_local(const size_t size);
void *alloc_global(const size_t size);
void dealloc(void *ptr);

#endif
