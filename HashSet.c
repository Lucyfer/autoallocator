#include "HashSet.h"

/// Private methods declaration

unsigned int hashset_hash(const HashSet *h, const char *elem);
unsigned int hashset_second_hash(const HashSet *h, const unsigned int previos, const unsigned int attempts);
bool hashset_find_index(const HashSet *h, const char *elem, unsigned int *index);
bool hashset_calc_index(const HashSet *h, const char *elem, unsigned int *index);
void hashset_realloc(HashSet *h);

/// Private methods implementation

unsigned int hashset_hash(const HashSet *h, const char *elem) {
    return (unsigned int)elem % h->size;
}

unsigned int hashset_second_hash(const HashSet *h, const unsigned int previos, const unsigned int attempts) {
    return (previos + ((unsigned)2 << attempts)) % h->size;
}

bool hashset_find_index(const HashSet *h, const char *elem, unsigned int *index) {
    if (!elem) {
        return false;
    }
    *index = hashset_hash(h, elem);
    if (h->elems[*index] && h->elems[*index] == elem) {
        return true;
    }  else {
        unsigned int i;
        for (i = 0; i < HASHSET_MAX_ATTEMPTS; ++i) {
            *index = hashset_second_hash(h, *index, i);
            if (h->elems[*index] && h->elems[*index] == elem) {
                return true;
            }
        }
    }
    return false;
}

bool hashset_calc_index(const HashSet *h, const char *elem, unsigned int *index) {
    if (!elem) {
        return false;
    }
    *index = hashset_hash(h, elem);
    if (!h->elems[*index]) {
        return true;
    } else {
        unsigned int i;
        for (i = 0; i < HASHSET_MAX_ATTEMPTS; ++i) {
            *index = hashset_second_hash(h, *index, i);
            if (!h->elems[*index]) {
                return true;
            }
        }
    }
    return false;
}

void hashset_realloc(HashSet *h) {
    if (h && h->elems && h->count >= h->size / 2) {
        char **oldBuf = h->elems;
        unsigned int oldSize = h->size;
        h->elems = (char **)calloc(h->size * 2, sizeof(char *));
        h->size *= 2;
        h->count = 0;
        unsigned int i;
        for (i = 0; i < oldSize; ++i) {
            if (oldBuf[i]) {
                hashset_insert(h, oldBuf[i]);
            }
        }
    }
}

/// Public methods

HashSet *hashset_alloc(void) {
    HashSet *h = (HashSet *)malloc(sizeof(HashSet));
    h->size = 0;
    h->count = 0;
    h->elems = NULL;
    return h;
}

HashSet *hashset_init(HashSet *h) {
    if (h) {
        h->elems = (char **)calloc(DEFAULT_HASHSET_SIZE, sizeof(char *));
        h->size = DEFAULT_HASHSET_SIZE;
        h->count = 0;
    }
    return h;
}

void hashset_clear(HashSet *h) {
    if (h) {
        free(h->elems);
        h->elems = NULL;
        h->size = 0;
        h->count = 0;
    }
}

void hashset_destroy(HashSet *h) {
    if (h) {
        hashset_clear(h);
        free(h);
    }
}

void hashset_insert(HashSet *h, char *elem) {
    if (!h) return;
    if (!h->elems || h->size == 0) {
        hashset_init(h);
    }
    if (h->count < h->size / 2) {
        unsigned int index;
        if (hashset_calc_index(h, elem, &index)) {
            h->elems[index] = elem;
            h->count++;
        } else {
            hashset_realloc(h);
            hashset_insert(h, elem);
        }
    } else {
        hashset_realloc(h);
        hashset_insert(h, elem);
    }
}

void hashset_remove(HashSet *h, const char *elem) {
    if (h) {
        unsigned int index;
        if (hashset_find_index(h, elem, &index)) {
            h->elems[index] = NULL;
            h->count--;
        }
    }
}

bool hashset_contains(const HashSet *h, const char *elem) {
    if (h) {
        unsigned int index;
        return hashset_find_index(h, elem, &index);
    }
    return false;
}

void hashset_foreach(const HashSet *h, VoidPtrHandler f) {
    if (h) {
        unsigned int i;
        for (i = 0; i < h->size; ++i) {
            if (h->elems[i]) {
                f(h->elems[i]);
            }
        }
    }
}
